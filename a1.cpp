#include <iostream>
#include <string>
#include <algorithm>
#include <tuple>

void print_result(std::tuple<char,int> input_tuple) {
    // Print out result from get_most_freq_letter
    std::cout << "['" << std::get<0>(input_tuple) << "', " << std::get<1>(input_tuple) << "]\n";
}

std::string get_unique_chars_in_string(std::string clear_string) {
    // Return string without char duplicates
    std::unique(clear_string.begin(), clear_string.end());
    std::sort(clear_string.begin(), clear_string.end());
    return clear_string;
}

std::tuple<char,int> get_most_freq_letter(std::string str) {
    // Return tuple (Most Frequent Letter, How many times it occurs)
    std::string unique_letters = get_unique_chars_in_string(str);
    char most_freq_letter = '0';
    int max_freq = 0;
    int temp_freq = 0;
    for (char letter : unique_letters) {
        temp_freq = std::count(str.begin(), str.end(), letter);
        if (temp_freq > max_freq) {
            max_freq = temp_freq;
            most_freq_letter = letter;
        }
    }
    return std::make_tuple(most_freq_letter, max_freq);
}

int main() {
    std::string str = "";
    getline(std::cin, str);
    print_result(get_most_freq_letter(str));
}
