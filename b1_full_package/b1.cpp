#include <iostream>
#include <string>
#include <list>

#include "customer.h"
#include "order.h"
#include "product.h"

#include "customer.cpp"
#include "order.cpp"
#include "product.cpp"

double get_total_price_of_orders_by_date(std::list<Order> orders, time_t orderDate) {
    double total_sum = 0;
    for (auto order : orders) {
        if (order.getOrderDate() == orderDate) {
            for (auto product : order.getProducts()) {
                total_sum += product.getPrice();
            }
        } 
    }
    return total_sum;
}

double get_total_price_of_order_by_date(Order orders, time_t orderDate) {
    double total_sum = 0;
    if (orders.getOrderDate() == orderDate) {
            for (auto product : orders.getProducts()) {
                total_sum += product.getPrice();
            }
        } 
    return total_sum;
}

int main()
{
    Customer customer = {0, "max", 0};
    std::set<Product> products;
    products.insert({0, "Paper", "Office", 100.0});
    Order order = {0, 2002, 203, "OK", customer, products};
    std::cout << get_total_price_of_order_by_date(order, 2002);
    std::list<Order> orders;
    orders.push_back(order);
    orders.push_back({1, 2002, 2043, "OK", customer, products});
    orders.push_back({2, 2000, 2003, "OK", customer, products});

    std::cout << get_total_price_of_orders_by_date(orders, 2002);
}