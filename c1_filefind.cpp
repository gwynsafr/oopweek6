#include <iostream>
#include <string>
#include <dirent.h>
#include <fstream>

std::string get_ext (const std::string& str) {
    size_t pos = str.rfind('.');
    if (pos <= 0) return "";
    return str.substr(pos+1, std::string::npos);
}

bool find_matching_file(DIR *dir, struct dirent *x, std::string find_target) {
    if((dir = opendir(".\\")) != NULL) {
        while ((x = readdir(dir)) != NULL) {
            if(find_target==x->d_name) {
                closedir (dir);
                std::cout << "FOUND";
                return true;
            }
        }
    }
    return false;
}

bool find_match_in_file (DIR *dir, struct dirent *x, std::string find_target) {
    std::ifstream data_store;
    std::string line;
    if ((dir = opendir (".\\")) != NULL) {
        while ((x = readdir (dir)) != NULL) {
            if(get_ext(x->d_name) == "txt") {
                data_store.open(x->d_name);
                while (getline(data_store, line)) {
                    if (line.find(find_target) != std::string::npos) {
                        closedir (dir);
                        std::cout << "FOUND";
                        return true;
                        }
                    }
            }
        }
    }
    return false;
}

bool find_in_current_dir(std::string find_target, std::string mode="file") {
    DIR *directory;
    struct dirent *x;
    if (mode == "file") {
        return find_matching_file(directory, x, find_target);
    }
    else if (mode == "content") {
        return find_match_in_file(directory, x, find_target);
    }
    else {return -1;}
}

// Напишите программу для поиска файлов по названию или содержимому (для текстовых файлов) в выбранной директории.

int main() {
    DIR *directory;   // creating pointer of type dirent
    struct dirent *x;   // pointer represent directory stream
    std::cout<<"Please enter file name"<< std::endl;  
    std::string target;
    std::cin >> target;
    std::cout<<"Please enter find mode (Search by file name by default)"<< std::endl;  
    std::string mode;
    std::cin >> mode;
    std::cout << find_in_current_dir(target, mode);
}
